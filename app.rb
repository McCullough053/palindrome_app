require 'sinatra'
require './palindrome.rb'

get '/' do
  @title = 'Home'
  erb :index
end

get '/about' do
  @title = 'About'
  erb :about
end

get '/palindrome-detector' do
  @title = 'Palindrome Detector'
  erb :palindrome
end

post '/check' do
  phrase_to_check = Phrase.new(params[:phrase])
  @phrase = phrase_to_check
  erb :result
end
