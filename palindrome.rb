# A simple setup to detect for palindromes- words spelled the same forwards and
# backwards

# Palindrome module contains shared code used for processing content and check-
# ing for palindrome status. Could be used mixed in with Integer and String, but
# I don't want to do that.
module Palindrome
  # Returns true for a palindrome and false otherwise
  def palindrome?
    # Will process and check for plaindrome if non-empty string.
    # Returns true/false for detection, returns nil for empty string
    length >= 1 ? processed_content == processed_content.reverse : nil
  end

  # Returns downcased chars for use in palindrome detection
  private

  def processed_content
    chars.map { |c| c.downcase.match(/\w/) }.join
  end
end

# Defines a Phrase class (inherits from String)
class Phrase < String
  include Palindrome
  def blank?
    match(/^\s*$/) ? true : false
  end

  def louder
    upcase # Calling upcase method on self here
  end
end

# Some test code to play around with some of the features built into Phrase
# phrase = Phrase.new("Madam, I'm Adam.")
# puts phrase
#
# phrase = Phrase.new('Able was I, ere I saw Elba.')
# puts phrase
# puts phrase.palindrome?
#
# phrase = Phrase.new('Racecar')
# puts phrase
# puts phrase.palindrome?
# puts phrase.louder

# Defines a translated Phrase
class TranslatedPhrase < Phrase
  attr_accessor :translation

  def initialize(content, translation)
    super(content) # Pass through content to Phrase
    @translation = translation # Store the translation
  end

  def processed_content
    @translation.downcase
  end
end

# Test code to play with the TranslatedPhrase functionality
# frase = TranslatedPhrase.new('recognize', 'reconocer')
# puts frase.palindrome?
